FROM gitpod/workspace-base:latest

# Install basic stuff
RUN sudo apt-get update \
    && sudo apt-get install -y gnupg software-properties-common curl git dirmngr gpg gawk \
    build-essential procps file libssl-dev lib32stdc++6 lib32z1 \
    && sudo rm -rf /var/lib/apt/lists/*

# Instal ASDF
RUN git clone https://github.com/asdf-vm/asdf.git $HOME/.asdf --branch v0.8.1
RUN echo ". $HOME/.asdf/asdf.sh" >> $HOME/.bashrc.d/asdf.sh
RUN echo ". $HOME/.asdf/completions/asdf.bash" >> $HOME/.bashrc.d/asdf.sh
COPY .tool-versions $HOME/
#RUN bash -c ". $HOME/.bashrc.d/asdf.sh && asdf plugin-add java && asdf install"
RUN bash -ic "asdf plugin-add java && asdf install"

# Install Flutter
RUN curl https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_2.2.3-stable.tar.xz > flutter.tar.xz
RUN tar -xf $HOME/flutter.tar.xz && rm $HOME/flutter.tar.xz
RUN echo 'export PATH="$PATH:$HOME/flutter/bin"' >> $HOME/.bashrc.d/flutter.sh
RUN bash -ic "flutter precache"

# Install Android SDK
ENV ANDROID_HOME=$HOME/Android
RUN mkdir $ANDROID_HOME
RUN curl https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip > $ANDROID_HOME/android-sdk.zip
RUN cd $ANDROID_HOME && unzip android-sdk.zip && mv cmdline-tools latest && mkdir cmdline-tools && mv latest cmdline-tools/
RUN echo "export ANDROID_HOME=$ANDROID_HOME" >> $HOME/.bashrc.d/android.sh
run echo "export ANDROID_SDK_ROOT=$ANDROID_HOME" >> $HOME/.bashrc.d/android.sh
RUN echo 'export PATH="$PATH:$ANDROID_HOME/cmdline-tools/latest/bin"' >> $HOME/.bashrc.d/android.sh
RUN echo 'export PATH="$PATH:$ANDROID_HOME/platform-tools"' >> $HOME/.bashrc.d/android.sh
RUN bash -ic "yes | sdkmanager 'platform-tools' 'platforms;android-28' 'platforms;android-30' 'build-tools;30.0.3'"
RUN bash -ic "yes | flutter doctor --android-licenses"





